from bottle import route, request, run, response


@route('/forward')
def fori():
    header_list = {}
    for header in request.headers:
        header_list[header]= request.headers.get(header)
    return str(header_list)

@route('/')
def index():
    header_list = {}
    for header in request.headers:
        header_list[header]= request.headers.get(header)
    print(header_list)
    print()
    print()
    print()
    response.status = 303
    response.set_header('Location', '/fori')
    response_list = {}
    for header in request.headers:
        if header != "Connection":
            response_list[header]=request.headers.get(header)
            response.set_header(header, request.headers.get(header))
    print(response_list)
    return response_list


run(host='0.0.0.0', port=8000, debug=True)
